#!/usr/bin/env python3
import numpy
import math
import itertools as it
import pyviennacl
import visvis as vv
import numpy.linalg
import sys
import time
import csv

class Timer(object):
    def __enter__(self):
        self.__start = time.time()

    def __exit__(self, type, value, traceback):
        # Error handling here
        self.__finish = time.time()

    def duration_in_seconds(self):
        return self.__finish - self.__start

X0, X1 = (-4.0, 4.0)
TYPE = numpy.float64

def update_grid(n):
    global N, WIDTH, S, DX, DX_SQ
    N = n
    WIDTH = X1-X0
    S = N+2
    DX = WIDTH/S
    DX_SQ = -1.0 / (DX*DX)

# Maps 0 to -2 and N to 2
def cmap(a):
    r = DX * a - X0
    return r

def solution(x, y):
    x, y = map(cmap, (x, y))
    return math.sin(x + y) * math.cos(y)

def rhs(x, y):
    x, y = map(cmap, (x, y))
    return (5.0 * math.sin(x + 2.0*y) + math.sin(x)) / 2.0

# Maps the domain index to vector
def vecmap(x, y):
    return y * S + x

def print_result(ref_sol, sol):
    for y in range(1, S):
        for x in range(1, S):
            ref = ref_sol[x,y]
            calc = sol[x,y]
            print(ref, calc, ref-calc)

def plot(ref, vals):
    s = vv.subplot(111)
    m1 = vv.surf((vals/2.0)+0.5)
    m1.colormap = vv.CM_JET
    m2 = vv.grid((ref/2.0)+0.5)
    m2.colormap = [(0.0, 1.0, 0.0)]
    s.daspect = 1.0, 1.0, 3.0 / DX

    app = vv.use()
    app.Run()

def direct_solve(op, gpurhs):
    gpuop = pyviennacl.Matrix(op)
    return pyviennacl.solve(gpuop, gpurhs, pyviennacl.upper_tag())

def iterative_solve(op, gpurhs):
    print('  building matrix...')
    #gpuop = pyviennacl.HybridMatrix(op)
    gpuop = op

    print('  configuring solver...')
    tag = pyviennacl.bicgstab_tag(tolerance = 1e-10, max_iterations = 10000)

    print('  solving...')
    timer = Timer()
    with timer:
        sol = pyviennacl.solve(gpuop, gpurhs, tag)
    print('      done in {} seconds'.format(timer.duration_in_seconds()))

    # Show some info
    print("Num. iterations: %s" % tag.iters)
    print("Estimated error: %s" % tag.error)

    return sol, tag.iters, timer.duration_in_seconds()

def compute():
    print('Domain {0}x{0}, from {1} to {2}:'.format(N, X0, X1))

    ref_sol = numpy.array([[solution(x, y) for x in range(S)] for y in range(S)])

    print("Building the RHS...")
    # Fill right-hand side with know solution
    vrhs = numpy.zeros(S*S, dtype=TYPE)
    for y in range(S):
        for x in range(S):
            vrhs[vecmap(x,y)] = rhs(x, y)

    print('Building the matrix...')
    #op = numpy.zeros((S*S, S*S), dtype=TYPE)
    op = pyviennacl.HybridMatrix((S*S, S*S))

    # For the boundaries, use the same value, because
    # they are taken from the analytical solution:
    for x, y in it.chain(*([(0, v),(S-1, v),(v, 0),(v, S-1)] for v in range(S))):
        idx = vecmap(x,y)
        op[idx, idx] = 1
        vrhs[vecmap(x,y)] = ref_sol[y,x]

    # For the middle, use the minus Laplacian
    for y in range(1,S-1):
        for x in range(1,S-1):
            idx = vecmap(x, y);
            op[idx, idx] = -4.0 * DX_SQ
            op[idx, vecmap(x-1, y)] = DX_SQ
            op[idx, vecmap(x+1, y)] = DX_SQ
            op[idx, vecmap(x, y-1)] = DX_SQ
            op[idx, vecmap(x, y+1)] = DX_SQ

    print("Solving...")

    ## GPU solvers:
    gpurhs = pyviennacl.Vector(vrhs)
    #sol = direct_solve(op, gpurhs)
    sol, iterations, solver_time = iterative_solve(op, gpurhs)
    sol = numpy.array([[sol[vecmap(x, y)].value for x in range(S)] for y in range(S)])

    ## Numpy direct solver:
    #sol = numpy.linalg.solve(op, vrhs)
    #sol = numpy.array([[sol[vecmap(x, y)] for x in range(S)] for y in range(S)])

    l2_norm = math.sqrt(sum(sum((ref_sol[1:-1,1:-1] - sol[1:-1,1:-1])**2))/(N*N))
    print("L2 norm:", l2_norm)

    return ref_sol, sol, l2_norm, iterations, solver_time

def main(ns):
    statsfile = open('stats.csv', 'w', newline='')
    stats = csv.writer(statsfile)
    stats.writerow(['N', 'Side', 'L2 norm', 'Iterations', 'Solver time (s)', 'Total time (s)'])
    for n in ns:
        update_grid(n)
        timer = Timer()
        with timer:
            ref_sol, sol, l2_norm, iterations, solver_time = compute()

        print('total case time: {}s\n'.format(timer.duration_in_seconds()))
        stats.writerow([n*n, n, l2_norm, iterations, solver_time, timer.duration_in_seconds()])
        statsfile.flush()
    
    #print_result(ref_sol, sol)
    plot(ref_sol, sol)

if __name__ == '__main__':
    try:
        ns = list(map(int, sys.argv[1:]))
        if not ns:
            ns = [128]
    except:
        ns = [128]
    
    main(ns)
